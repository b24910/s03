// activity 1 - Quiz

// 1.
"classes"


// 2.
"pascalcase"



// 3.
"new"



// 4.
"instantiating a class"



// 5.
"contructor"


// Activity 1 - Functional coding

// 1.

// class Student {
// 	constructor(name, email, arr) {
// 		this.name = name;
// 		this.email = email;
//         const rightNum = (currentValue) =>{
//             return currentValue < 101 && currentValue > 0 && (typeof currentValue === "number") && arr.length === 4
//         };
//         this.grades = arr.every(rightNum)? arr : undefined
// 	}
// }

class Student {
	constructor(name, email, grades) {
		//propertyName = value
		this.name = name;
		this.email = email;
		//add this property to the constructor
		this.gradeAve = undefined;
		this.grades = undefined;
        this.studentWillPass = undefined;
        this.withHonors = undefined;

        // activity 1
		const rightNum = (currentValue) =>{ 
            return currentValue < 101 && currentValue > 0 && (typeof currentValue === "number") && grades.length === 4
        };
        this.grades = grades.every(rightNum)? grades : undefined
	}

	login() {
		console.log(`${this.email} has logged in`);
		return this;
	}
	logout() {
		console.log(`${this.email} has logged out`);
		return this
	}
	listGrades(){
		console.log(`${this.name}'s quarterly grade averages are: ${this.grades}`);
		return this;
	}
	computeAve() {
		let sum = 0;
		this.grades.forEach(grade => sum = sum + grade);
		//update property
		this.gradeAve = sum/4;
		//return object
		return this;
	}
	willPass() {
        this.studentWillPass = this.gradeAve >= 85 ? true : false;
		return this;
	}

	willPassWithHonors() {
        const getWithHonors = () => {
            if(this.willPass()) {
                if(this.computeAve() >= 90) {
                    return true;
                } else {
                    return false;
                }
            } else {
                return undefined;
            }
        }
        this.withHonors = getWithHonors();
		return this
	}
}

let studentOne = new Student('john', 'john@mail.com',[89, 84, 78, 88]);
let studentTwo = new Student('Joe', 'joe@mail.com',[78, 82, 79, 85]);
let studentThree = new Student('Jane', 'jane@mail.com',[87, 89, 91, 93]);
let studentFour = new Student('jessie', 'john@mail.com',[91, 89, 92, 93]);
console.log(studentOne);
console.log(studentTwo);
console.log(studentThree);
console.log(studentFour);

console.log(studentOne.login().computeAve().willPass().willPassWithHonors().logout())



// Activity 2

// 1. no



// 2. no



// 3. yes



// 4. method chaining



// 5. this



